/* eslint-disable react-refresh/only-export-components */
import { lazy } from "react";

const Login = lazy(() => import("../pages/login"));
const Home = lazy(() => import("../pages/home"));

//新增
const User = lazy(() => import("../pages/user"));

const Manage = lazy(() => import("../pages/manage"));

//修改
const routes = [
  {
    path: "/login",
    element: <Login />,
  },
  {
    element: <Home />,
    children: [
      {
        path: "/",
        element: <User />,
      },
      {
        path: "/manage",
        element: <Manage />,
      },
    ],
  },
  {
    path: '*',
    element: <div>404</div>,
  },
];

export default routes;
